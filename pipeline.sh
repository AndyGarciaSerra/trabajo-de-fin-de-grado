#----------------------------------------------------------------------------------------#
#! /bin/sh
#
# This shell script is the pipeline regarding the detection of low surface brightness
# satellite galaxies. It is part of a final bachelor thesis on the detections of these
# objects around NGC1042.
#
# Copyright (C) 2021 Andrés García-Serra Romero <alu0101451923@ull.edu.es>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.


# Exit the script in the case of failure
set -e

# Save the current system language, and then change it to English to avoid
# problems in some parts of the code (AWK with `,' instead of `.' for
# decimal separator).
system_lang=$LANG
export LANG=C





# Used shell packages:
#   gnuastro (latest version, <https://www.gnu.org/software/gnuastro/#installation>)
#   parallel





# Important data:
    # Directories:
      DATADIR=''        #Directory with the dataset
      DETDIR=''         #Directory in which the detections will be made

    # General information of the image:
      zeropoint_r=33.1112
      zeropoint_g=33.5274
      date=$(date +%F)
      hour=$(date +%H)
      minute=$(date +%M)
      dataset='edr3'

    # Pixelsize and size of image in pixels:
      pixelsize=$(astfits "$DATADIR"/g.fits -h0 \
                --pixelscale --quiet | awk '{print $1}')
      aperture=$(echo $pixelsize | awk '{print $1 *5}')
      pixelsizearcsec=$(echo $pixelsize | awk '{print $1*3600}')
      pixelsx=$(astfits $DATADIR/g.fits -h0 | grep 'NAXIS1' | awk '{print $3}')
      pixelsy=$(astfits $DATADIR/g.fits -h0 | grep 'NAXIS2' | awk '{print $3}')
      pixelsxy="$pixelsx"x"$pixelsy"





# Version control:
  croppdfdir="$DETDIR"/versions
  versionnopardir="$croppdfdir"/no-criteria
  versionrangeddir="$croppdfdir"/ranged
    if [ -d "$croppdfdir" ]; then
      echo "version/ exists"
    else
      mkdir $croppdfdir
      mkdir $versionnopardir
      mkdir $versionrangeddir
    fi





# Weighted image:
  for f in g r; do
    astarithmetic "$DATADIR"/latest-$f.fits \
                  "$DATADIR"/latest-$f_weight.fits -g1 15 lt nan where \
                  --out=latest-"$f".fits;
  done





# Noise chisel:
  chiseldir="$DETDIR"/chisel
  if [ -d "$chiseldir" ]; then
      echo "chisel/ exists"
  else
    mkdir $chiseldir

    # Chiselling:
    for f in g r; do
      astnoisechisel "$DATADIR"/latest-$f.fits -h1 \
                     --output="$chiseldir"/nc-"$f".fits \
                     --tilesize=30,30 \
                     --interpnumngb=50 \
                     --meanmedqdiff=0.01;
    done
  fi





# Kernel:
  kerneldir="$DETDIR"/kernel
  if [ -d "$kerneldir" ]; then
    echo "kernel/ exists"
  else
    mkdir $kerneldir
    astmkprof --kernel=gaussian,3,5 --oversample=1 \
              --output="$kerneldir"/kernel-fat.fits
    astmkprof --kernel=gaussian,2,5 --oversample=1 \
              --output="$kerneldir"/kernel.fits
  fi





# Segmentation:
  segdir="$DETDIR"/seg
  if [ -d "$segdir" ]; then
     echo "seg/ exists"
  else
    mkdir $segdir
    for f in g r; do
      astsegment "$chiseldir"/nc-"$f".fits -hINPUT-NO-SKY \
  	             --kernel="$kerneldir"/kernel-fat.fits \
                 --output="$segdir"/seg-"$f".fits;
    done
  fi





# Catalogs:
  catdir="$DETDIR"/catalog
  regdir="$catdir"/reg

  if [ -d "$catdir" ]; then
      echo "catalog/ exists"
  else
    mkdir $catdir
    mkdir $regdir

    # Making catalogs:
      astmkcatalog "$segdir"/seg-g.fits --ids --ra --dec --magnitude --halfsumsb \
                   --surfacebrightness --halfsumarea --halfmaxarea --halfmaxsb --halfsumradius \
                   --zeropoint="$zeropoint_g" --clumpscat --output="$catdir"/cat-g.fits
      astmkcatalog "$segdir"/seg-g.fits --ids --ra --dec --magnitude --halfsumsb \
                   --surfacebrightness --halfsumarea --halfmaxarea --halfmaxsb --halfsumradius \
                   --valuesfile="$chiseldir"/nc-r.fits --zeropoint="$zeropoint_r" --clumpscat \
                   --output="$catdir"/cat-r.fits

    # Formatting:
      asttable "$catdir"/cat-r.fits -hCLUMPS --output="$catdir"/r-clumps.txt \
               -c1,2,3,4,5,6,7,8,9,10,11 \
               --colmetadata=HOST_OBJ_ID,ID --colmetadata=ID_IN_HOST_OBJ,sub-ID \
               --colmetadata=MAGNITUDE,r-MAG,log,"Magnitude on r-band." \
               --colmetadata=HALF_SUM_SB,r-SBhs,mag/arcsec²,"Effective SB in r-band." \
               --colmetadata=SURFACE_BRIGHTNESS,r-SB,mag/arcsec² \
               --colmetadata=HALF_SUM_RADIUS,r-Rhs --colmetadata=HALF_SUM_AREA,r-Ahs \
               --colmetadata=HALF_MAX_SB,r-SBhm,mag/arcsec² \
               --colmetadata=HALF_MAX_AREA,r-Ahm,"Radius at Half of the Maximum pixel value"
      asttable "$catdir"/cat-g.fits -hCLUMPS --output="$catdir"/g-clumps.txt \
               -c1,2,3,4,5,6,7,8,9,10,11 \
               --colmetadata=HOST_OBJ_ID,ID --colmetadata=ID_IN_HOST_OBJ,sub-ID \
               --colmetadata=MAGNITUDE,g-MAG,log,"Magnitude on g-band." \
               --colmetadata=HALF_SUM_SB,g-SBhs,mag/arcsec²,"Effective SB in g-band." \
               --colmetadata=SURFACE_BRIGHTNESS,g-SB,mag/arcsec² \
               --colmetadata=HALF_SUM_RADIUS,g-Rhs --colmetadata=HALF_SUM_AREA,g-Ahs \
               --colmetadata=HALF_MAX_SB,g-SBhm,mag/arcsec² \
               --colmetadata=HALF_MAX_AREA,g-Ahm,"Radius at Half of the Maximum pixel value"

    # Merging catalogs:
      asttable "$catdir"/g-clumps.txt --output="$catdir"/gr-clumps.txt \
               --catcolumnfile="$catdir"/r-clumps.txt --catcolumnrawname
      asttable "$catdir"/gr-clumps.txt --output="$catdir"/gr-clumps.txt \
               -c1,2,3,4,5,6,7,8,9,10,11,16,17,18,19,20,21,22

    # Magnitude, SBhs & Rhs range set:
      asttable "$catdir"/gr-clumps.txt --output="$catdir"/gr-clumps-ranged.txt \
      --range=r-MAG,"$rmagrange" --range=g-MAG,"$gmagrange" --range=r-SBhs,"$rSBhsrange" \
      --range=g-SBhs,"$gSBhsrange" --range=g-Rhs,"$gRhsrange" --range=r-Rhs,"$rRhsrange" \
      --range=g-MAG,"$gmagrange" --range=g-SBhs,"$gSBhsrange" --range=g-Rhs,"$gRhsrange" \

    # Making a catalog of GAIA sources in our image:
      astquery gaia --dataset=edr3 --overlapwith="$DATADIR"/g.fits -h0 \
      -csource_id,ra,dec --output="$catdir"/gaia.fits

    # Substracting GAIA sources from catalogs:
      astmatch "$catdir"/gr-clumps.txt         "$catdir"/gaia.fits \
                                                             --hdu2=QUERY \
               --ccol1=RA,DEC                                --ccol2=ra,dec \
               --aperture=$aperture --notmatched \
               --output="$catdir"/gr-clumps-ranged-substracted.txt

    # Cleaning:
      rm  "$catdir"/gr-clumps-ranged-substracted_matched_2.txt
      mv  "$catdir"/gr-clumps-ranged-substracted_matched_1.txt \
          "$catdir"/gr-clumps-ranged-substracted.txt
fi





# Reg file for visual inspection:
if [ -n "$1" ] && [ $1 == "-reg" ]; then
  asttable "$catdir"/gaia.fits -h1 > "$catdir"/gaia-objects.txt
  awk 'BEGIN{print "# Region file format: DS9 version 4.1";      \
      print "global color=green width=2";                 \
      print "fk5";}                                       \
      !/^#/{printf "circle(%s,%s,1\") # text={%s}\n",$3,$4,$1;}'\
      "$catdir"/gr-clumps-ranged-substracted.txt > "$regdir"/udgs.reg
  awk 'BEGIN{print "# Region file format: DS9 version 4.1";      \
      print "global color=red width=2";                 \
      print "fk5";}                                       \
      !/^#/{printf "circle(%s,%s,1\") # text={%s}\n",$2,$3,$1;}' \
      "$catdir"/gaia-objects.txt > "$regdir"/gaia.reg
  ds9 -mecube "$segdir"/seg-g.fits -zscale -zoom to fit -regions load all "$regdir"/gaia.reg \
      -regions load all "$regdir"/udgs.reg &
  rm "$catdir"/gaia-objects.txt
else
  echo "No reg file requested, to do so please type '-reg' after the running script command."
fi





# Cropping the stamps:
  gaiaobjects=$(asttable "$catdir"/gaia.fits -i | grep 'rows' | awk '{print $4}')
  udgnum=$(asttable "$catdir"/gr-clumps-ranged-substracted.txt -i | grep 'rows' | \
         awk '{print $4}')
  cropdir="$DETDIR"/crops
  if [ -d "$cropdir" ]; then
     echo "crops/ exists"
  else
    mkdir $cropdir

    # Creating a parameter list page for the detections-pdf:
        echo "
                        CATALOGUE INFORMATION:

        Detections made on "$date", "$hour":"$minute".
        Original image:                           NGC-1042
        Pixelsize (arcsec):                       "$pixelsizearcsec"
        Number ox pixels:                         "$pixelsxy"

        Noisechisel:
        Tilesize:                               "$tilesize"
        Smoothwidth:                            "$smoothwidth"
        Minskyfrac:                             "$minskyfrac"

        Segmentation:
        Snquant:                                "$snquant"

        Magnitudes range:                         r-> "$rmagrange"
        (mag)                                     g-> "$gmagrange"

        SB on half maximum range:                 r-> "$rSBhsrange"
        (mag/arcsec²)                             g-> "$gSBhsrange"

        Radius on half maximum:                   r-> "$rRhsrange"
        (arcsec)                                  g-> "$gRhsrange"

        Gaia sources detected on original image:  "$gaiaobjects"
        Gaia dataset used:                        "$dataset"
        Number of UDG candidates on this catalog: "$udgnum"">> "$croppdfdir"/parameters.txt

        pandoc "$croppdfdir"/parameters.txt -o "$croppdfdir"/parameters.pdf
        rm "$croppdfdir"/parameters.txt

    # Ranged-substracted crops and pdf:
      echo 'Cropping ranged-substracted catalog...'
      astcrop $DATADIR/g.fits -h0 --mode=wcs --coordcol=RA --coordcol=DEC --quiet \
              --catalog="$catdir"/gr-clumps-ranged-substracted.txt --width=15/3600,15/3600 --suffix=-g.fits \
              --output="$cropdir"
      cd $cropdir
      parallel astconvertt --colormap=viridis --invert -ojpg ::: *.fits
      img2pdf *.jpg --output="$versionrangeddir"/images.pdf
      pdftk "$croppdfdir"/parameters.pdf "$versionrangeddir"/images.pdf cat output \
            "$versionrangeddir"/detections_"$date"_"$hour":"$minute".pdf
      rm "$croppdfdir"/parameters.pdf "$versionrangeddir"/images.pdf
      cd $DETDIR
fi
