#----------------------------------------------------------------------------------------#
#
# This script creates the selection of the region of interest regarding possible LSBG in
# LBT data. It is part of a final bachelor thesis on the detections of these
# objects around NGC1042.
#
# Copyright (C) 2021 Andrés García-Serra Romero <alu0101451923@ull.edu.es>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# Lines regarding the manual classification between Sharp and Diffuse objects
# are commented so they can be changed or deleted later on.
#





##IMPORTING##
import os
import matplotlib.pyplot as plt
import numpy as np





##FORMAT##
plt.rc('font', size=20)          # controls default text sizes
plt.rc('axes', titlesize=20)     # fontsize of the axes title
plt.rc('axes', labelsize=20)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=16)    # fontsize of the tick labels
plt.rc('ytick', labelsize=16)    # fontsize of the tick labels





##FILES##
files = os.listdir(".")
files2 = []
files3 = []
files4 = []
#sharp & diffuse----
files5 = []
files6 = []
#-------------------





##IMPORTING##
for name in files:
    if name[:3] == "cat":
        files2.append(name)
for name in files:
    if name[:3] == "udg":
        files3.append(name)
for name in files:
    if name[:3] == "obj":
        files4.append(name)
#Sharp & diffuse-------------------------------------
for name in files:
    if name[:3] == "sha":
        files5.append(name) #Importing sharp sources
for name in files:
    if name[:3] == "dif":
        files6.append(name) #Importing difuse objects
#----------------------------------------------------





##PRINT FOR CHECKING##
print(files2)
print(files3)
print(files4)
#Sharp & diffuse--
print(files5)
print(files6)
#-----------------





##SELECTION LINES DEFINITION##
xm = 18
xM = 24.5
p1 = [18,22]
p2 = [24,27.3]
deltay = p2[1]-p1[1]
deltax = p2[0]-p1[0]
m = deltay/deltax
delta = 1.4





##PLOTTING LOOP##
for ii,file in enumerate(files3):
    data = np.loadtxt(file)
    xudg = data[:,0]
    yudg = data[:,1]

    data = np.loadtxt(files2[ii])
    xcat = data[:,0]
    ycat = data[:,1]

    data = np.loadtxt(files4[ii])
    xobj = data[:,0]
    yobj = data[:,1]

    #Sharp & diffuse--------------
    data = np.loadtxt(files5[ii])
    xeli = data[:,0]
    yeli = data[:,1]

    data = np.loadtxt(files6[ii])
    xdif = data[:,0]
    ydif = data[:,1]
    #-----------------------------

    xlinea = xcat
    ylinea1 = m*(xlinea-p1[0])+p1[1]
    ylinea2 = m*(xlinea-p1[0])+p1[1]-delta

    plt.figure(figsize=(7,6))
    ax = plt.gca()

    #Annotations:
    #bbox_props = dict(boxstyle="round,pad=0.2", fc="w", ec="k", lw=1)
    #n = ["DF1","SDSSJ024...","LSB21","LBT1"]
    #for i, txt in enumerate(n):
    #    ax.annotate(txt,xy=(xudg[i],yudg[i]),xytext=(xudg[i]+0.1,
    #                        yudg[i]+0.1),bbox=bbox_props,color='red',
    #                        fontsize=15)

    #Catalog:
    plt.plot(xcat,ycat,"k.",ms=2,label="Detections Catalog")

    #Lines:
    plt.plot(xlinea,ylinea1,'g',linewidth=1)
    plt.plot(xlinea,ylinea2,'g',linewidth=1)
    plt.axvline(x=xm,color='g',linewidth=1)
    plt.axvline(x=xM,color='g',linewidth=1)

    #Sharp & diffuse--------------------------------------------------
    plt.plot(xeli,yeli,"m.",ms=6,label="Sharp sources")
    plt.plot(xdif,ydif,"y.",ms=6,label="Diffuse objects")
    #-----------------------------------------------------------------

    #Some LSBG examples ("Example objects") and UDGs from paper matched
    #with my catalog ("My faint objects"):
    plt.plot(xudg,yudg,"c*",ms=8,label="LSB objects")
    plt.plot(xobj,yobj,"b*",ms=8,label="Example objects")

    #Plot formatting:
    lgnd=plt.legend(fontsize=14)
    plt.ylabel("Half Max SB "+r"$(\frac{mag}{arcsec²})$")
    plt.xlabel("Magnitude in "+file[-5:-4]+" band")
    lgnd.legendHandles[0]._legmarker.set_markersize(10)
    #Sharp & diffuse--------------------------------------------------
    lgnd.legendHandles[1]._legmarker.set_markersize(10)
    lgnd.legendHandles[2]._legmarker.set_markersize(10)
    #-----------------------------------------------------------------
    lgnd.legendHandles[3]._legmarker.set_markersize(10)
    lgnd.legendHandles[4]._legmarker.set_markersize(10)
    plt.axis([17,30,18,28])


    #Saving plots:
    plt.savefig(file[-5:-4]+"-band.png")
    #Sharp & diffuse--------------------------------------------------
    plt.savefig("plot_sharp&diffuse.png")
    #-----------------------------------------------------------------





##REGION SELECTING##
data = np.loadtxt(files2[ii])
datacut = data[(xcat>xm) & (xcat<xM) & (ycat>ylinea2) & (ycat<ylinea1),:]
#Saving catalogue of possible objects:
np.savetxt('possible-ob.txt',datacut)
